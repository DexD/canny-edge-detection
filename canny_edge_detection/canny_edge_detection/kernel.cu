#include <stdio.h>
#include <assert.h>
#include <cuda.h>
#include "bmp.h"
#include <cmath>
#include <string.h>

unsigned char * cannyEdgeDetection ( sImage slika );


/*Sirina slike mora biti deljiva sa 32*/
#define TILE_WIDTH		32
#define PI		3.1415


/*Menjati po potrebi*/
#define UPPER_THRESHOLD 3
#define LOWER_THRESHOLD 1

#define FILTER_GAUSS 1
#define FILTER_GX 3
#define FILTER_GY 3

static void HandleError( cudaError_t err, const char *file, int line );
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


static void greska (char* s){
	fprintf(stderr,"%s", s);
	getchar();
	exit(EXIT_FAILURE);
}


__device__ __constant__ float GausFilter[25] ={2, 4, 5, 4, 2, 4, 9, 12, 9, 4, 5, 12, 15, 12, 5, 4, 9, 12, 9, 4, 2, 4, 5, 4, 2};
__device__ __constant__ float GxFilter[9] = {-1, 0, 1, -2, 0, 2, -1, 0, 1};
__device__ __constant__ float GyFilter[9] = {1, 2, 1, 0, 0, 0, -1, -2, -1};

__global__ void konvulacija(unsigned char* dataDevice, unsigned char* rezultatDevice, int widht, int height, char filter, int dimFiltera);
__global__ void edgeDirectionAndMagnitude(unsigned char * Gx, unsigned char * Gy, int width, unsigned char * rezultat_dir, unsigned char * rezultat_str );
__global__ void klasifikacijaPravaca(unsigned char * data, int width,unsigned char * rezultat);
__global__ void nonmaximumSupression(unsigned char * data, unsigned char * mag, int width, int height, unsigned char * rezultat);
__global__ void hysteresis(unsigned char * data, unsigned char * mag, unsigned char *res, int width, int height, unsigned char * rezultat);

unsigned char * cannyEdgeDetection ( sImage slika )
{


	unsigned char* data = slika.data;
	int height = slika.rows;
	int width = slika.cols;

	unsigned char * Gx, *Gy;
	Gx = (unsigned char*) malloc(width*height*sizeof(char));
	Gy = (unsigned char*) malloc(width*height*sizeof(char));
	if(Gx ==NULL || Gy == NULL)
		greska("Greska pri alociranju memorije za Gx i Gy matrice.\n");
	unsigned char * dataDevice, * rezultatDevice, *dataDeviceMag, *rezultatDeviceMag, *dataDeviceResult, *rezultatDeviceResult;


	unsigned char * ptr = (unsigned char * ) malloc(width*height*sizeof(char));

	HANDLE_ERROR(cudaMalloc((void**)&dataDevice, width*height*sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&dataDeviceMag, width*height*sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&dataDeviceResult, width*height*sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&rezultatDevice, width*height*sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&rezultatDeviceMag, width*height*sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&rezultatDeviceResult, width*height*sizeof(char)));

	/* primenjuje se konvulacija sa Gausovim filterom */
	HANDLE_ERROR(cudaMemcpy(dataDevice, data, width*height*sizeof(char), cudaMemcpyHostToDevice));

	dim3 dimGrid(ceil((float)width/TILE_WIDTH),ceil((float)height/TILE_WIDTH));
	dim3 dimBlock(TILE_WIDTH,TILE_WIDTH);

	konvulacija<<<dimGrid, dimBlock>>>(dataDevice, rezultatDevice, width, height, 'g', FILTER_GAUSS);
	cudaThreadSynchronize();

	HANDLE_ERROR(cudaMemcpy(data, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));


	/* primenjuje se konvulacija sa Gx filterom i rezultat se smesta u niz int-ova Gx */
	HANDLE_ERROR(cudaMemcpy(dataDevice, data, width*height*sizeof(char), cudaMemcpyHostToDevice));

	konvulacija<<<dimGrid, dimBlock>>>(dataDevice, rezultatDevice, width, height, 'x', FILTER_GX);
	cudaThreadSynchronize();

	HANDLE_ERROR(cudaMemcpy(Gx, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));



	/* primenjuje se konvulacija sa Gy filterom i rezultat se smesta u niz int-ova Gy */
	HANDLE_ERROR(cudaMemcpy(dataDevice, data, width*height*sizeof(char), cudaMemcpyHostToDevice));

	konvulacija<<<dimGrid, dimBlock>>>(dataDevice, rezultatDevice, width, height, 'y', FILTER_GY);
	cudaThreadSynchronize();

	HANDLE_ERROR(cudaMemcpy(Gy, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));



	/* theta sadrzi pravce u svakoj tacki */
	unsigned char * theta;
	theta = (unsigned char*)malloc(width*height*sizeof(char));
	if(theta == NULL)
		greska("Greska prilikom alociranja memorije za theta.\n");

	/*mag sadrzi magnitude piksela u svakoj tacki*/
	unsigned char * mag;
	mag = (unsigned char*)malloc(width*height*sizeof(char));
	if(mag == NULL)
		greska("Greska prilikom alociranja memorije za mag.\n");

	/*res sadrzi rezultantni piksel u svakoj tacki*/
	unsigned char * res;
	res = (unsigned char*)malloc(width*height*sizeof(char));
	if(res == NULL)
		greska("Greska prilikom alociranja memorije za res.\n");


	/*cmp_res sluzi za proveru promena slike*/
	unsigned char * cmp_res;
	cmp_res = (unsigned char*)malloc(width*height*sizeof(char));
	if(cmp_res == NULL)
		greska("Greska prilikom alociranja memorije za cmp_res.\n");



	unsigned char *Gx_device, *Gy_device;


	HANDLE_ERROR(cudaMalloc((void**)&Gx_device, width*height*sizeof(char)));
	HANDLE_ERROR(cudaMalloc((void**)&Gy_device, width*height*sizeof(char)));


	HANDLE_ERROR(cudaMemcpy(Gx_device, Gx, width*height*sizeof(char), cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(Gy_device, Gy, width*height*sizeof(char), cudaMemcpyHostToDevice));

	edgeDirectionAndMagnitude<<<dimGrid, dimBlock>>>(Gx_device, Gy_device, width, rezultatDevice, rezultatDeviceMag);
	cudaThreadSynchronize();

	HANDLE_ERROR(cudaMemcpy(theta, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(mag, rezultatDeviceMag, width*height*sizeof(char), cudaMemcpyDeviceToHost));

		/*oslobadjanje memorije */
	HANDLE_ERROR(cudaFree(rezultatDeviceMag));
	HANDLE_ERROR(cudaFree(Gx_device));
	HANDLE_ERROR(cudaFree(Gy_device));

	HANDLE_ERROR(cudaMemcpy(dataDevice, theta, width*height*sizeof(char), cudaMemcpyHostToDevice));

	klasifikacijaPravaca<<<dimGrid, dimBlock>>>(dataDevice, width, rezultatDevice);
	cudaThreadSynchronize();
	
	HANDLE_ERROR(cudaMemcpy(theta, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));


	HANDLE_ERROR(cudaMemcpy(dataDevice, theta, width*height*sizeof(char), cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dataDeviceMag, mag, width*height*sizeof(char), cudaMemcpyHostToDevice));

	nonmaximumSupression<<<dimGrid, dimBlock>>>(dataDevice, dataDeviceMag, width, height, rezultatDevice);
	cudaThreadSynchronize();
	
	HANDLE_ERROR(cudaMemcpy(res, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));


	
		
		memcpy(cmp_res, res, sizeof(res));
		bool repeat = true;
		while (repeat == true)
		{
			HANDLE_ERROR(cudaMemcpy(dataDevice, theta, width*height*sizeof(char), cudaMemcpyHostToDevice));
			HANDLE_ERROR(cudaMemcpy(dataDeviceMag, mag, width*height*sizeof(char), cudaMemcpyHostToDevice));
			HANDLE_ERROR(cudaMemcpy(dataDeviceResult, res, width*height*sizeof(char), cudaMemcpyHostToDevice));

			hysteresis<<<dimGrid, dimBlock>>>(dataDevice, dataDeviceMag, dataDeviceResult, width, height, rezultatDevice);
			cudaThreadSynchronize();

			HANDLE_ERROR(cudaMemcpy(res, rezultatDevice, width*height*sizeof(char), cudaMemcpyDeviceToHost));

			if (memcmp(res, cmp_res,sizeof(res)) == 0) repeat = false;

			memcpy(cmp_res, res, sizeof(res));		
		}

		/* oslobadjanje memorije */
		HANDLE_ERROR(cudaFree(dataDevice));
		HANDLE_ERROR(cudaFree(dataDeviceMag));
		HANDLE_ERROR(cudaFree(dataDeviceResult));
		HANDLE_ERROR(cudaFree(rezultatDevice));
		HANDLE_ERROR(cudaFree(rezultatDeviceResult));

		free(Gx);
		free(Gy);
		free(mag);
		free(theta);

		
#ifdef ISPIS
	int i;
	for (i=0; i<width*height; i++)
		printf("%d\t", mag[i]);		 
#endif
	return res;
}



/* ********************************************************************** */

__global__ void konvulacija(unsigned char* dataDevice, unsigned char* rezultatDevice, int width, int height, char filter, int dimFiltera){

	int x = threadIdx.x;
	int y = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;


	int i, j;
	float pom=0;
	int indeks = by*32*width + y*width + bx * 32 + x;
	
	switch(filter){
			case 'g':
			/*primenjuje se GausFilter */

	for (i=-dimFiltera/2; i<= dimFiltera/2; i++){
		for(j=-dimFiltera/2; j<=dimFiltera/2; j++){

			int vrednost=0;

			if( indeks + j*width + i <0 || indeks + j*width +i > width*height)
				vrednost = 0;
			else if((indeks + j*width)/width != (indeks+j*width +i)/width)
				vrednost = 0;

			else vrednost =(int) dataDevice[indeks+j*width+i];
			
				pom += GausFilter[(j+dimFiltera/2)*dimFiltera+(i+dimFiltera/2)]/159 * vrednost ;
				
				
			}
		}
		break;
		case 'x':
		/*primenjuje se GxFilter */

		for (i=-dimFiltera/2; i<= dimFiltera/2; i++){
		for(j=-dimFiltera/2; j<=dimFiltera/2; j++){

			int vrednost=0;

			if( indeks + j*width + i <0 || indeks + j*width +i > width*height)
				vrednost = 0;
			else if((indeks + j*width)/width != (indeks+j*width +i)/width)
				vrednost = 0;

			else vrednost =(int) dataDevice[indeks+j*width+i];
			
				pom += GxFilter[(j+dimFiltera/2)*dimFiltera+(i+dimFiltera/2)] * vrednost ;
				
				
			}
		}
		break;
		case 'y':
		/*primenjuje se GyFilter */

		for (i=-dimFiltera/2; i<= dimFiltera/2; i++){
		for(j=-dimFiltera/2; j<=dimFiltera/2; j++){

			int vrednost=0;

			if( indeks + j*width + i <0 || indeks + j*width +i > width*height)
				vrednost = 0;
			else if((indeks + j*width)/width != (indeks+j*width +i)/width)
				vrednost = 0;

			else vrednost =(int) dataDevice[indeks+j*width+i];
			
				pom += GyFilter[(j+dimFiltera/2)*dimFiltera+(i+dimFiltera/2)] * vrednost ;
				
				
			}
		}
		break;
	}
	

	rezultatDevice[indeks] =(char) pom;


}


/* ********************************************************************** */

__global__ void edgeDirectionAndMagnitude(unsigned char * Gx, unsigned char * Gy, int width, unsigned char * rezultat_dir, unsigned char * rezultat_mag){

	int x = threadIdx.x;
	int y = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;

	int indeks = by*32*width + y*width + bx * 32 + x;

	float pom = 180/PI * atan2((float)Gy[indeks], (float)Gx[indeks]);
	rezultat_dir[indeks]=(char)pom;

	float mag = sqrt(powf((float)Gy[indeks], 2) + powf( (float)Gx[indeks], 2));
	rezultat_mag[indeks]=(char)mag;

}


/* ********************************************************************** */

__global__ void  klasifikacijaPravaca(unsigned char * data, int width,unsigned char * rezultat){

	int x = threadIdx.x;
	int y = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;

	int indeks = by*32*width + y*width + bx * 32 + x;
	int ugao = (int)data[indeks];

	char pom;
	if(ugao < 22)
		pom = 0;
	else if(ugao < 67)
		pom = 45;
	else if (ugao < 112)
		pom = 90;
	else if (ugao < 157)
		pom = 135;
	else if (ugao>157)
		pom = 0;


	rezultat[indeks]=pom;

}


/* ********************************************************************** */
__global__ void nonmaximumSupression(unsigned char * data, unsigned char * mag, int width, int height,unsigned char * rezultat){

	int x = threadIdx.x;
	int y = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;

	int indeks = by*32*width + y*width + bx * 32 + x;
	int pravac = (int)data[indeks]; 
	int magnituda = (int)mag[indeks];
	int p1;
	int p2;

	char pom = 0;

	switch(pravac)
	{

	case 0:{
		if (x > 0) 
		{ 
			p1 = (int)mag[indeks-1]; 
			if ( magnituda < p1 ) break;
		}
		if (x < width) 
		{ 
			p2 = (int)mag[indeks+1]; 
			if ( magnituda < p2 ) break;
		}
		if ( magnituda < UPPER_THRESHOLD) break;

		/*Jeste ivica*/
		pom = 255;
		   } break;


	case 45:{
		if (x > 0 && y > 0) 
		{ 
			p1 = (int)mag[indeks-width-1]; 
			if ( magnituda < p1 ) break; 
		}
		if (x < width && y < height) 
		{ 
			p2 = (int)mag[indeks+width+1]; 
			if ( magnituda < p2 ) break; 
		}
		if ( magnituda < UPPER_THRESHOLD) break; 

		/*Jeste ivica*/
		pom = 255;
			} break;


	case 90:{
		if (y > 0) 
		{ 
			p1 = (int)mag[indeks-width]; 
			if ( magnituda < p1 ) break;
		}
		if (y < height) 
		{ 
			p2 = (int)mag[indeks+width]; 
			if ( magnituda < p2 ) break;
		}
		if ( magnituda < UPPER_THRESHOLD) break;

		/*Jeste ivica*/
		pom = 255;
			} break;


	case 135:{
		if (x < width && y > 0) 
		{ 
			p1 = (int)mag[indeks-width+1]; 
			if ( magnituda < p1 ) break; 
		}
		if (x > 0 && y < height) 
		{ 
			p2 = (int)mag[indeks+width-1]; 
			if ( magnituda < p2 ) break; 
		}
		if ( magnituda < UPPER_THRESHOLD) break; 

		/*Jeste ivica*/
		pom = 255;
			 } break;
	}


	rezultat[indeks] = pom;
}


/* ********************************************************************** */

__global__ void hysteresis(unsigned char * pravci, unsigned char * mag, unsigned char *res, int width, int height, unsigned char * rezultat)
{
	int x = threadIdx.x;
	int y = threadIdx.y;

	int bx = blockIdx.x;
	int by = blockIdx.y;

	int indeks = by*32*width + y*width + bx * 32 + x;
	int pravac = (int)pravci[indeks]; 
	int magnituda;
	int p1;
	int p2;
	char pom = 0;


	int ivica = (int)res[indeks];
	if (ivica == 255){


		switch(pravac)
		{
		case 0:{

			if (x > 0) 
			{			
				if ( (int)pravci[indeks-1] != 0 ) break;
				if ( (int)mag[indeks-1] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks-1];


				if (x-1 > 0) 
				{ 
					p1 = (int)mag[indeks-2]; 
					if ( magnituda < p1 ) break;
				}


				if (x-1 < width) /*skloniti uslov*/ 
				{ 
					p2 = (int)mag[indeks]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks-1] = 255;
			} 

			if (x < width) 
			{ 
				if ( (int)pravci[indeks+1] != 0 ) break;
				if ( (int)mag[indeks+1] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks+1];

				if (x+1 > 0)/*skloniti uslov*/ 
				{ 
					p1 = (int)mag[indeks]; 
					if ( magnituda < p1 ) break;
				}
				if (x+1 < width) 
				{ 
					p2 = (int)mag[indeks+2]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks+1] = 255;
			} 
			   } break;





		case 45:{
			if (x > 0 && y > 0) 
			{	
				if ( (int)pravci[indeks-width-1] != 0 ) break;
				if ( (int)mag[indeks-width-1] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks-width-1];

				if (x-1 > 0 && y-1 > 0) 
				{ 
					p1 = (int)mag[indeks-width*2-2]; 
					if ( magnituda < p1 ) break;
				}
				if (x-1 < width && y-1 < height) /*skloniti uslov*/
				{ 
					p2 = (int)mag[indeks]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks-width-1] = 255;
			} 

			if (x < width && y < height) 
			{ 
				if ( (int)pravci[indeks+width+1] != 0 ) break;
				if ( (int)mag[indeks+width+1] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks+width+1];

				if (x+1 > 0 && y+1 > 0) /*skloniti uslov*/
				{ 
					p1 = (int)mag[indeks]; 
					if ( magnituda < p1 ) break;
				}
				if (x+1 < width && y+1 < height) 
				{ 
					p2 = (int)mag[indeks+width*2+2]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks+width+1] = 255;
			} 
				}break;


		case 90:{
			if (y > 0) 
			{	
				if ( (int)pravci[indeks-width] != 0 ) break;
				if ( (int)mag[indeks-width] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks-width];

				if (y-1 > 0) 
				{ 
					p1 = (int)mag[indeks-width*2]; 
					if ( magnituda < p1 ) break;
				}
				if (y-1 < height) /*skloniti uslov*/
				{ 
					p2 = (int)mag[indeks]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks-width] = 255;
			} 

			if (y < height) 
			{ 
				if ( (int)pravci[indeks+width] != 0 ) break;
				if ( (int)mag[indeks+width] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks+width];

				if (y+1 > 0) /*skloniti uslov*/
				{ 
					p1 = (int)mag[indeks]; 
					if ( magnituda < p1 ) break;
				}
				if (y+1 < height) 
				{ 
					p2 = (int)mag[indeks+width*2]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks+width] = 255;

			} 
				}break;


		case 135:{
			if (x < width && y > 0) 
			{	
				if ( (int)pravci[indeks-width+1] != 0 ) break;
				if ( (int)mag[indeks-width+1] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks-width+1];

				if (x+1 < width && y-1 > 0) 
				{ 
					p1 = (int)mag[indeks-width*2+2]; 
					if ( magnituda < p1 ) break;
				}
				if (x+1 > 0 && y-1 < height) /*skloniti uslov*/
				{ 
					p2 = (int)mag[indeks]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks-width+1] = 255;
			} 

			if (x-1 > 0 && y+1 < height) 
			{ 
				if ( (int)pravci[indeks+width-1] != 0 ) break;
				if ( (int)mag[indeks+width-1] > LOWER_THRESHOLD ) break;
				magnituda = (int)mag[indeks+width-1];

				if (x-1 < width && y+1 > 0) /*skloniti uslov*/
				{ 
					p1 = (int)mag[indeks]; 
					if ( magnituda < p1 ) break;
				}
				if (x-1 > 0 && y+1 < height) 
				{ 
					p2 = (int)mag[indeks+width*2-2]; 
					if ( magnituda < p2 ) break;
				}

				if ( magnituda < UPPER_THRESHOLD) break;

				/*Jeste ivica*/
				rezultat[indeks+width-1] = 255;
			} 
				 }break;
		}
	}



	
}


/* ********************************************************************** */

static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}