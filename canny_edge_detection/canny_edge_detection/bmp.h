typedef struct {int rows; int cols; unsigned char* header; unsigned char* data;} sImage;

/* cita konkternu informaciju sa ulaznog toka, pocevsi od pozicije offset, velicine numberOfChars bajttova */
extern 
long getImageInfo(FILE* inputFile, long offset, int numberOfChars);

/* cita bitove hedera, ukupno 54+4*nColors  */
extern unsigned char * readHeader(FILE* input, int n);

/* ispisuje zadati niz karaktera sa n elemenata, ako je mod=='c' ispisuje ih kao karaktere, ako je mod=='d' ispisuje ih kao intove */
extern void printCharArray(unsigned char* niz, int n, char mod);

/*  zapisuje sliku na zadatu adresu */
extern void saveImage(unsigned char * header, int nh, unsigned char * data, int nd, char* adresa);

extern unsigned char* cannyEdgeDetection(sImage slika);

