#include <stdio.h>
#include <stdlib.h>
#include <cmath>

static void greska (char* s);


/* ------------- citanje hedera --------------*/
unsigned char * readHeader(FILE* input, int n){

	unsigned char * p = (unsigned char*) malloc (n*sizeof(char));
	
	if(p==NULL)
		greska("Nije moguce alocirati memoriju za smestanje hedera.\n");	

	fseek(input, 0, SEEK_SET);

	size_t rezultat;
	rezultat = fread((void*)p, 1, n, input);
	if(rezultat != n)
		greska("Greska pri citarnju hedera.\n");

	return p;
}


/* ------------- ispis niza charova --------------*/

void printCharArray(unsigned char* niz, int n, char mod){

int i;

	switch(mod){
	case 'c':
		for (i=0; i<n; i++)
			printf("%c \t", niz[i]);
		break;
	case 'd':
		for (i=0; i<n; i++)
			printf("%d \t", niz[i]);
		break;
}
	
}


/* ------------- ispis u BMP fajl --------------*/

void saveImage(unsigned char * header, int nh, unsigned char * data, int nd, char* adresa){

	FILE* output;
	
	 output = fopen ( adresa , "wb" );
	 if(output == NULL)
		greska("Neuspesno pisanje u fajl.\n");

	 fwrite((void*)header, sizeof(char), nh, output);
	 fwrite((void*)data, sizeof(char), nd, output);
	 fclose(output);
	 printf("Slika je smestena na adresu: %s", adresa);
}



/*----------GET IMAGE INFO SUBPROGRAM--------------*/


long getImageInfo(FILE* inputFile, long offset, int numberOfChars)
{
  unsigned char			*ptrC;
  long				value = 0L;
  unsigned char			dummy;
  int				i;

  dummy = '0';
  ptrC = &dummy;

  fseek(inputFile, offset, SEEK_SET);

  for(i=1; i<=numberOfChars; i++)
  {
    fread(ptrC, sizeof(char), 1, inputFile);
    /* calculate value based on adding bytes */
    value = (long)(value + (*ptrC)*((long) powl(256, (i-1))));
  }
  return(value);

} 


/* ------------- obrada greske --------------*/
static void greska (char* s){
fprintf(stderr,"%s", s);
getchar();
exit(EXIT_FAILURE);
}