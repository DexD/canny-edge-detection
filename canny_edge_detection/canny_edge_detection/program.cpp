#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include "bmp.h"




static sImage		originalImage;

static void greska(char* s);
static void unesiAdresu(char* adresa);

int main(int argc, char* argv[])
{
  FILE			*bmpInput, *rasterOutput;
  sImage		originalImage;
  unsigned char		someChar;
  unsigned char*	pChar;
  int			nColors;
  long			fileSize;
  int			vectorSize, r, c;
  int			kompresija;


#define MAX_KARAKTERA	500
  char adresaIn[MAX_KARAKTERA];
  char adresaOut[MAX_KARAKTERA];


  /* initialize pointer */
  someChar = '0';
  pChar = &someChar;
  printf ("Uneti adresu slike:	");
  unesiAdresu(adresaIn);
 
 

  /*--------citanje input fajla------------*/
  bmpInput = fopen(adresaIn, "rb");
  if(bmpInput == NULL)
	  greska("Neuspesno citanje fajla.\n");
  fseek(bmpInput, 0L, SEEK_END);
  rasterOutput = fopen("data.txt", "w");

  /*--------GET BMP DATA---------------*/

  originalImage.cols = (int)getImageInfo(bmpInput, 18, 4);
  originalImage.rows = (int)getImageInfo(bmpInput, 22, 4);
  fileSize = getImageInfo(bmpInput, 2, 4);
  nColors = getImageInfo(bmpInput, 46, 4);
  vectorSize = fileSize - (14 + 40 + 4*nColors);
  kompresija = getImageInfo(bmpInput, 30, 4);

  if(kompresija != 0)
	  greska("Program ne podrzava kompresovane slike. \n");

  printf("Width: %d\n", originalImage.cols);
  printf("Height: %d\n", originalImage.rows);
  printf("File size: %ld\n", fileSize);
  printf("# Colors: %d\n", nColors);
  printf("Vector size: %d\n", vectorSize);


  originalImage.header = readHeader(bmpInput, 14 + 40 + 4*nColors);
#ifdef	ISPIS
  printCharArray(originalImage.header, 54+4*nColors, 'd');
#endif
  /*----------READ RASTER DATA---------*/
 
 unsigned char*  slika  = (unsigned char*)malloc(originalImage.cols*originalImage.rows);
  int i=0;

  fseek(bmpInput, (54 + 4*nColors), SEEK_SET);

  for(r=0; r<=originalImage.rows - 1; r++)
  {
    for(c=0; c<=originalImage.cols - 1; c++)
    {
      fread(pChar, sizeof(char), 1, bmpInput);
	  slika[i]=*pChar;
	i++;
      fprintf(rasterOutput, "(%d, %d) = %d\n", r, c, *pChar);
    }
  }

  fclose(bmpInput);
  fclose(rasterOutput);

  originalImage.data = slika;
  int pom = originalImage.cols*originalImage.rows;
  //printf("\n%d\n", pom);


  originalImage.data = cannyEdgeDetection(originalImage);
  //printCharArray(originalImage.data, pom, 'd');

  
   printf ("Uneti adresu izlaznog fajla:	\n");
  unesiAdresu(adresaOut);
 
 
  saveImage(originalImage.header,54+4*nColors,  originalImage.data , pom,  adresaOut);

  getchar();
  return EXIT_SUCCESS;

}


/* ------------- obrada greske --------------*/
static void greska (char* s){
fprintf(stderr,"%s", s);
getchar();
exit(EXIT_FAILURE);
}

/* -----------------------------------------*/

static void unesiAdresu(char* adresa){

	int i=0;
	char c = getchar();

	while (c != '\n'){
		adresa[i] = c;
		c = getchar();
		i++;
	}

	adresa[i] = '\0';
	
	


}